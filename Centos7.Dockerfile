from centos:7.4.1708

VOLUME [ "/output" ]

RUN rpm --rebuilddb
RUN yum install -y rpm-build rpm-devel rpmlint make python bash coreutils diffutils patch rpmdevtools
RUN rpm --rebuilddb
RUN yum install -y gcc-c++ gcc-gfortran
RUN rpm --rebuilddb

WORKDIR /workdir

RUN rpmdev-setuptree
ADD devenv-cmake.spec /workdir/devenv-cmake.spec
ADD build_rpm /workdir/build_rpm
RUN chmod 755 /workdir/build_rpm

CMD [ "/workdir/build_rpm" ]