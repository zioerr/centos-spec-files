# Based on https://github.com/open-io/rpm-specfiles/tree/master/cmake
# Set to bcond_without or use --with bootstrap if bootstrapping a new release
# or architecture
%bcond_with bootstrap
# Set to bcond_with or use --without gui to disable qt4 gui build
%bcond_without gui
# --with tests to run unit tests
%bcond_with tests
# Set to RC version if building RC, else %{nil}
%define rcver %{nil}
%define devenv_prefix /opt/pgs/devenv
%define realname cmake
%define _prefix /opt/pgs/devenv
%define _datadir %{_prefix}/share
%define _bindir %{_prefix}/bin
%define _mandir %{_prefix}/man

Name:           devenv-cmake
Version:        3.9.6
Release:        1%{?dist}
Summary:        Cross-platform make system

Group:          Development/Tools
License:        BSD
URL:            http://www.cmake.org
Source0:        http://www.cmake.org/files/v3.9/cmake-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gcc-c++

%if %{without bootstrap}
BuildRequires:  ncurses-devel, libX11-devel
BuildRequires:  curl-devel, expat-devel, zlib-devel xz-devel
BuildRequires: xmlrpc-c-devel
%endif
%if %{with gui}
BuildRequires: qt4-devel, desktop-file-utils
%define qt_gui --qt-gui
%endif
Requires:       rpm


%description
CMake is used to control the software compilation process using simple
platform and compiler independent configuration files. CMake generates
native makefiles and workspaces that can be used in the compiler
environment of your choice. CMake is quite sophisticated: it is possible
to support complex environments requiring system configuration, pre-processor
generation, code generation, and template instantiation.


%package        gui
Summary:        Qt GUI for %{name}
Group:          Development/Tools
Requires:       %{name} = %{version}-%{release}

%description    gui
The %{name}-gui package contains the Qt based GUI for CMake.


%prep
%setup -q -n cmake-%{version}
# Fixup permissions
find -name \*.h -o -name \*.cxx -print0 | xargs -0 chmod -x


%build
export CFLAGS="$RPM_OPT_FLAGS"
export CXXFLAGS="$RPM_OPT_FLAGS"
./bootstrap --prefix=%{devenv_prefix} --datadir=/share/%{realname} \
            --docdir=/share/doc/%{realname}-%{version} --mandir=/share/man \
            --%{?with_bootstrap:no-}system-libs \
            --parallel=`/usr/bin/getconf _NPROCESSORS_ONLN` \
            %{?qt_gui}
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT/%{_datadir}/%{realname}/Modules -type f | xargs chmod -x
mkdir -p $RPM_BUILD_ROOT%{_datadir}/emacs/site-lisp

%if %{with gui}
# Desktop file
desktop-file-install --delete-original \
  --dir=%{buildroot}%{_datadir}/applications \
  %{buildroot}/%{_datadir}/applications/CMake.desktop
%endif


%if %{with tests}
%check
unset DISPLAY
bin/ctest -V
%endif

%clean
rm -rf $RPM_BUILD_ROOT


%if %{with gui}
%post gui
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :

%postun gui
update-desktop-database &> /dev/null || :
update-mime-database %{_datadir}/mime &> /dev/null || :
%endif


%files
%defattr(-,root,root,-)
%{_datadir}/doc/%{realname}-%{version}/
%{_bindir}/cmake
%{_bindir}/cpack
%{_bindir}/ctest
%{_datadir}/%{realname}/
%{_datadir}/aclocal/cmake.m4
#%{_mandir}/man1/*.1*
#%{_datadir}/emacs/

%if %{with gui}
%files gui
%defattr(-,root,root,-)
%{_bindir}/cmake-gui
%{_datadir}/applications/CMake.desktop
%{_datadir}/mime/packages/cmakecache.xml
%{_datadir}/pixmaps/CMakeSetup.png
%endif


%changelog
# nothing to report
